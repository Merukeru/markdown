# Markdown
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

This file is a demo for markdown.

## Headlines

This is a H1
============
code:
```
This is a H1
============
```

This is a H2
------------
code:
```
This is a H2
------------
```

# This is a H1
code:
`# This is a H1`

## This is a H2
code:
`## This is a H2`

### This is a H3
code:
`### This is a H3`

#### This is a H4
code:
`#### This is a H4`

##### This is a H5
code:
`##### This is a H5`

###### This is a H6
code:
`###### This is a H6`


Also possible

# This is a H1 #
code:
`# This is a H1 #`

## This is a H2 ##
code:
`## This is a H2 ##`

### This is a H3 ###
code:
`### This is a H3 ###`

#### This is a H4 ####
code:
`#### This is a H4 ####`

##### This is a H5 #####
code:
`##### This is a H5 #####`

###### This is a H6 ######
code:
`###### This is a H6 ######`


## Paragraphs
* A paragraph consist of one or more lines
* Paragraphs are separated by one or more empty lines. Empty lines are lines without any characters or consist only of tabs or spaces.

## Line breaks
Line breaks are created by using the return key. New lines are foced by adding two or more spaces at the end of a text lines.

## Citation
Citation is done by using the `>` sign at the beginning of a line.

> This is a citation


code:
```
> This is a citation 
```

> This is a multiline citation. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere
> lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet
> vitae, risus.


code:
```
> This is a multiline citation. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere
> lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet
> vitae, risus. 
```


Alternative way:
> This is a multiline citation. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere
lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet
vitae, risus.


code:
```
> This is a multiline citation. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere
lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet
vitae, risus. 
```


There can be multiple levels of citations

> First citation level
>
> > Nested citation
>
> Back to first level

code:
```
> First citation level
>
> > Nested citation
>
> Back to first level
```


## Lists

### Unsorted Lists
Unsorted lists are created by using the `*`, `-` or `+` characters at the beginning of a line

List 1
- Item 1
- Item 2
- Item 3

code:
```
- Item 1
- Item 2
- Item 3
```


List 2
- Item 1
* Item 2
+ Item 3

code:
```
- Item 1
* Item 2
+ Item 3
```


### Sorted Lists

Use numbers to sorted create lists. It does not matter if you use always the same number.

List 3
1. Item 1
1. Item 2
1. Item 3

code:
```
1. Item 1
1. Item 2
1. Item 3
```


If there are empty lines between list points, then the items will be wrapped in <p> tags

List 4
- Item 1

- Item 2

code:
```
- Item 1

- Item 2
```


For multiline list items you have to consider the alignment.

List 5

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

code:
```
-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
```


Alternative way:

List 6

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

code:
```
-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
```


For nested citations you have to use the same alignment (one tab or 4 spaces)

-   Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	> Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

code:
```
-   Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

	> Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
```


To prevent the creation of a list you can use a backslash

100\. Birthday

code:
```
100\. Birthday
```


## Code blocks

Code blocks are created by encapsulating a text in singe ` characters.

example
'this is code'

code:
```
	'this is code'
```


For multiline code blocks use three ` characters

example
```
this is code
```

code:
```
	```
	'this is code'
	```
```


An alternative is to use 4 spaces or a tab at the beginning of a line

Lorem ipsum dolor sit amet, consetetur sadipscing elitr
	this is code

code:
```
Lorem ipsum dolor sit amet, consetetur sadipscing elitr
	this is code
```


For nested code blocks in lists you have to use double indention (two tabs or 8 spaces)

-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr

        ```
		this is code
		```

code:
```
-	Lorem ipsum dolor sit amet, consetetur sadipscing elitr

        ```
		this is code
		```
```


## Horizontal lines

Horizontal lines are created by three or more stars or hyphens in a line. For example: `***`, `* * *`, `---`, `- - -`, ``*****`, `-----------`

example:
***

code:
```
***
```




Collected and summarized by [merukeru](http://merukeru.bitbucket.org/)